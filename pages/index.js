import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import Link from 'next/link'
export default function Home() {
  return (
    <>
      <Head>
        <title>Article List | Home</title>
        <meta name="keywords" content="articles"/>
      </Head>
      <h1 className={styles.title}>Home</h1>
      <p className={styles.text}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean neque nibh, vestibulum non dignissim ac, mattis sagittis lorem. Nulla ullamcorper aliquet luctus. Integer in odio augue. Morbi faucibus elementum scelerisque. Pellentesque viverra turpis ut neque accumsan molestie. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur faucibus odio vel lacinia feugiat. Ut facilisis luctus ipsum id pharetra. Duis vulputate felis dapibus vestibulum scelerisque. Cras vehicula eros ut lobortis cursus. Integer luctus diam sed ligula condimentum pellentesque.
      </p>
      <Link href="/articles">
        See Article Listing
      </Link>
    </>
  )
}
