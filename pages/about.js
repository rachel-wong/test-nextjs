import React from 'react'
import Head from 'next/head'
const About = () => {
  return (
    <>
      <Head>
        <title>Article List | Home</title>
        <meta name="keywords" content="articles"/>
      </Head>

      <div>
      <h1>About</h1>
      </div>
    </>
  )
}

export default About
