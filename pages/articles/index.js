import styles from '../../styles/Articles.module.css'
import Link from 'next/link'

// runs at build time, never runs at browser.
// this runs before comkponent is rendered
export const getStaticProps = async () => {
  const res = await fetch("https://jsonplaceholder.typicode.com/users")
  const data = await res.json()
  return {
    props: { // this defines the props for the index coponent
      articles: data // this is available inside the INdex component here as props
    }
  }
}
import React from 'react'

const Index = ({ articles }) => {
    console.log(articles)

  return (
    <div>
      <h1>Article test</h1>
      {articles.map((item, idx) => (
        <Link href={ "/articles/" + item.id} key={item.id}>
          <a className={styles.single}>
            <h3>{ item.name }</h3>
          </a>
        </Link>
      ))}
    </div>
  )
}

export default Index
