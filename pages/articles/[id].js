
// // fetch all data in this function to next so it needs to be async/await
// export const getStaticPaths = async () => {
//   const res = await fetch("https://jsonplaceholder.typicode.com/users")
//   const data = await res.json()

//   // get a list of id
//   const paths = data.map(item => {
//     return {
//       params: { id: item.id.toString() }
//     }
//   })
//   return {
//     paths,
//     fallback: false // fallback pages to show 404 page
//   }
// }

// // this runs for every page inside the params paths array
// export const getStaticProps = async (context) => {
//   const id = context.params.id // gets the id of the article that we want
//   const res = await fetch("https://jsonplaceholder.typicode.com/users" + id)
//   const data = await res.json()
//   console.log("data", data)
//   return {
//     props: { article: data}
//   }
// }

// const Details = ({ article }) => {
//   console.log("article", article)
//   return (
//     <div>
//       <h1>Details Page</h1>
//       <p>{article.name}</p>
//       <p>{ article.name }</p>
//       <p>{ article.name }</p>

//     </div>
//   )
// }

// export default Details

// how many pages need to be made
export const getStaticPaths = async () => {
  const res = await fetch('https://jsonplaceholder.typicode.com/users');
  const data = await res.json();

  // map data to an array of path objects with params (id)
  const paths = data.map(article => {
    return {
      params: { id: article.id.toString() }
    }
  })

  return {
    paths,
    fallback: false
  }
}
// for each of the page, it runs this and put into the components
export const getStaticProps = async (context) => {
  const id = context.params.id;
  const res = await fetch('https://jsonplaceholder.typicode.com/users/' + id);
  const data = await res.json();

  return {
    props: { article: data }
  }
}

const Details = ({ article }) => {
  return (
    <div>
      <h1>{ article.name }</h1>
      <p>{ article.email }</p>
      <p>{ article.website }</p>
      <p>{ article.address.city }</p>
    </div>
  );
}

export default Details;