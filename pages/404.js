import Link from 'next/link';
import { useEffect } from 'react'
import { useRouter } from 'next/router' // very similar to react-router-dom

const NotFound = () => {

  const router = useRouter() // has to sit outside of useEffect because it can't run just the one time

  useEffect(() => {

    setTimeout(() => {
      //redirect user
      // router.go() // go back and forth through the history using -1, +1
      router.push("/")
    }, 3000)
  }, []);
  return (
    <div>
      <div className="not-found">
        <h1>Page not found here!</h1>
        <p>Page cannot be found</p>
        <Link href="/">Go back to homepage</Link>
      </div>
    </div>
  )
}

export default NotFound
