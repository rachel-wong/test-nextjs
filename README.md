
## Learning notes

#### Routing
* every page is its own component inside /pages and is tied to the route (route automatically creates a route)
* index.js would be named as "/" root route

codesplitting - only serve javascript when it lands on the page. each page has its own bundle and only served when navigated for the first time.

#### Layout
* Creates a separate wrapping parent layout to wrap around all the content instead of placing things in the _app.js
* need to pass in `{ children }`


### STyling modules
* Scope styles to specific pages/components
* avoids classname conflict
* modular css (hom.emodule.css)
* import the css into the responsible component `import styles from '../styles/Home.module.css'`
* refer to the class as `styles.container` or `styles.whatevercssclass`
* the selectors in modular css must be class selectors, not tag selectors

### Custom 404
* Just need to create a 404.js under /pages

### Redirecting users
* need to use useEffect()

### Images
* no need to import images into components as static assets
* can use Image from next instead of `<img>`, and makes it responsive, lazy-loaded

### Metadata
* head from next/head can allow you to add a <Head> specifying the title, metadata for every page component and

#### Calling api

* getStaticProps used to grab data instead of useEffect because components are rendered before it reaches browser so the data must arrive before then.
* it defines the props, async/await


### dynamic routes
* use [] around the name of the file meaning it is changeable