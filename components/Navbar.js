import Link from 'next/link'
import Image from 'next/image'
const Navbar = () => {
  return (
    <nav>
      <div className="logo">
        <Image width={128} height={ 77} className="logo-image" src="/logo.webp" />
      </div>
      <Link href="/">Home</Link>
      <Link href="/about">About</Link>
      <Link href="/articles">Article Listing</Link>
    </nav>
  )
}

export default Navbar
